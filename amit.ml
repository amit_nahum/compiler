(* compiler.ml
 * A compiler from Scheme to CISC
 *
 * Programmer: Mayer Goldberg, 2015
*)

#use "pc.ml";;

let counter_ref = ref 200;;

exception X_not_yet_implemented;;
exception X_this_should_not_happen;;

let rec ormap f s =
  match s with
  | [] -> false
  | car :: cdr -> (f car) || (ormap f cdr);;

let rec andmap f s =
  match s with
  | [] -> true
  | car :: cdr -> (f car) && (andmap f cdr);;	  

let string_to_list str =
  let rec loop i limit =
    if i = limit then []
    else (String.get str i) :: (loop (i + 1) limit)
  in
  loop 0 (String.length str);;

let list_to_string s =
  let rec loop s n =
    match s with
    | [] -> String.make n '?'
    | car :: cdr ->
      let result = loop cdr (n + 1) in
      String.set result n car;
      result
  in
  loop s 0;;

type fraction = {numerator : int; denominator : int};;

type number =
  | Int of int
  | Fraction of fraction;;

type sexpr =
  | Void
  | Bool of bool
  | Nil
  | Number of number
  | Char of char
  | String of string
  | Symbol of string
  | Pair of sexpr * sexpr
  | Vector of sexpr list;;

module type SEXPR = sig
  val sexpr_to_string : sexpr -> string
end;; (* signature SEXPR *)

module Sexpr : SEXPR = struct

  exception X_invalid_fraction of fraction;;

  let bool_to_string =
    function
    | true -> "#t"
    | false -> "#f"
  ;;

  let number_to_string =
    function
    | (Int x) -> (string_of_int x)
    | (Fraction {numerator = x ; denominator = y}) ->
      if x mod y == 0 
      then (string_of_int (x/y))
      else
        let x_as_string = string_of_int x in
        let y_as_string = string_of_int y in
        x_as_string ^ "/" ^ y_as_string
  ;;

  let normalize_scheme_symbol str =
    let s = string_to_list str in
    if (andmap
	  (fun ch -> (ch = (Char.lowercase ch)))
	  s) then str
    else Printf.sprintf "|%s|" str;;

  let sexpr_to_string sexpr =
    let rec foo sexpr = 
      match sexpr with
      | Void -> "" 
      | Bool x -> bool_to_string x
      | Nil -> "'()"
      | Number x -> number_to_string x
      | Char x ->
          (match x with
           | '\010'-> "#\\newline"
           | '\013' ->"#\\return"
           | '\009' -> "#\\tab"
           | '\012' -> "#\\page"
           | '\032' -> "#\\space"
           | _ -> "#\\" ^ (Char.escaped x))
      | String x -> x
      | Symbol x -> "\'" ^ x
      | Pair (x,y) -> "'(" ^ pair_to_string (x,y) ^ ")"
      | Vector list -> "#(" ^ vector_to_string list ^ ")"

    and vector_to_string =
      function
      | [] -> ""
      | hd::[] -> (foo hd)
      | hd::tail -> (foo hd) ^ " " ^ (vector_to_string tail)

    and pair_to_string =
      function
      |(Pair (x,y),Pair (w,z)) -> (pair_to_string (x,y))  ^ " " ^ (pair_to_string (w,z)) 
      |(x,Pair (y,z)) ->
        (match x with
         | Symbol "quasiquote" -> "\096" ^ (pair_to_string (y,z))
         | Symbol "unquote" -> "\044" ^ (pair_to_string (y,z))
         | Symbol "unquote-splicing" -> ",@" ^ (pair_to_string (y,z))
         | Symbol x -> x ^ " " ^ (pair_to_string (y,z)) 
         | _ -> (foo x) ^ " " ^ (pair_to_string (y,z)))
      |(Pair (x,z),y) ->
        (match y with
         | Nil -> (pair_to_string (x,z))
         | _ -> (pair_to_string (x,z))  ^" "^ (foo y))
      |(x,Nil) ->
        (match x with
         | Symbol x -> x
         | _ -> (foo x))
      |(x,y) ->  (foo x)^" . "^(foo y)


    in
    foo sexpr
  ;;

end;; (* struct Sexpr *)

module type PARSER = sig
  val read_sexpr : string -> sexpr
  val read_sexprs : string -> sexpr list
end;;

module Parser : PARSER = struct

  open PC;;

  let make_char_value base_char displacement =
    let base_char_value = Char.code base_char in
    fun ch -> (Char.code ch) - base_char_value + displacement;;

  let nt_bool =
    let nt = word_ci "#f" in
    let nt = pack nt (fun _ -> Bool false) in
    let nt' = word_ci "#t" in
    let nt' = pack nt' (fun _ -> Bool true) in
    disj nt nt';;

  let nt_int =
    let nt = char '-' in
    let nt = pack nt (fun e -> -1) in
    let nt' = char '+' in
    let nt' = pack nt' (fun e -> 1) in
    let nt = disj nt nt' in
    let nt = maybe nt in
    let nt = pack nt (function | None -> 1 | Some(mult) -> mult) in

    let nt' = range '0' '9' in
    let nt' = pack nt' (make_char_value '0' 0) in

    let nt' = plus nt' in
    let nt' = pack nt' (fun s -> List.fold_left (fun a b -> a * 10 + b) 0 s) in

    let nt = caten nt nt' in
    let nt = pack nt (fun (mult, n) -> (mult * n)) in
    nt;;

  let nt_int_hex =
    let nt = char '-' in
    let nt = pack nt (fun e -> -1) in
    let nt' = char '+' in
    let nt' = pack nt' (fun e -> 1) in
    let nt = disj nt nt' in
    let nt = maybe nt in
    let nt = pack nt (function | None -> 1 | Some(mult) -> mult) in

    let nt' = range '0' '9' in
    let nt' = pack nt' (make_char_value '0' 0) in

    let nt'' = range 'a' 'f' in
    let nt'' = pack nt'' (make_char_value 'W' 0) in

    let nt''' = range 'A' 'F' in
    let nt''' = pack nt''' (make_char_value '7' 0) in

    let nt' = disj_list [nt'; nt'';nt'''] in


    let nt' = plus nt' in
    let nt' = pack nt' (fun s -> List.fold_left (fun a b -> a * 16 + b) 0 s) in

    let nt'' = word_ci "0x" in
    let nt'' = caten nt'' nt' in
    let nt'' = pack nt'' (fun (_,n) -> n) in

    let nt = caten nt nt'' in
    let nt = pack nt (fun (mult, n) -> (mult * n)) in
    nt;;

  let nt_integer =
    let nt  = pack nt_int (fun x -> Number (Int x)) in
    let nt' = pack nt_int_hex (fun x -> Number (Int x)) in
    let nt  = disj nt' nt in
    nt;;

  let notzero num =
    if num <> 0 then true else false;;

  let nt_fraction =
    let foo nt1 nt2 =
      pack
        (caten (pack (caten nt1 (char '/')) (fun (x,_) -> x)) (guard nt2 notzero))
        (fun (x,y) -> if x mod y <> 0
          then (Number (Fraction {numerator = x ; denominator = y}))
          else (Number (Int (x/y)))
        )
    in
    disj_list [(foo nt_int nt_int);
               (foo nt_int nt_int_hex);
               (foo nt_int_hex nt_int);
               (foo nt_int_hex nt_int_hex)];;

  let rec nt_symbol s =
    let nt   = range_ci 'a' 'z' in
    let nt'  = range '0' '9' in
    let nt'' = one_of "!$^*-_=+<>/?" in
    let nt   = plus (disj_list [nt ;nt';nt'']) in
    let packs = (disj
                   (pack  nt_end_of_input (fun(_) -> true))
                   (pack (diff nt_any nt_symbol)  (fun(_) -> true))) in
    let nt_int_out = caten nt_integer packs in
    let nt_frac_out = caten nt_fraction packs in
    let nt   = diff nt nt_int_out in
    let nt   = diff nt nt_frac_out in
    let nt   = pack nt (fun x -> (Symbol (String.lowercase (list_to_string x)))) in
    nt s;;


  let nt_meta =
    let nt1 = pack (word "\\n") (fun _ -> '\n') in
    let nt2 = pack (word "\\r") (fun _ -> '\r') in
    let nt3 = pack (word "\\t") (fun _ -> '\t') in
    let nt4 = pack (word "\\f") (fun _ -> Char.chr 12) in
    let nt5 = pack (word "\\\\") (fun _ -> '\\') in
    let nt6 = pack (word "\\\"") (fun _ -> '\"') in
    let nt1 = disj_list [nt1;nt2;nt3;nt4;nt5;nt6] in
    nt1;;

  let nt_string =  
    let nt = caten (char '\"') (star (disj (diff nt_any (one_of "\\\"")) (nt_meta))) in
    let nt = caten nt (char '\"') in
    let nt = pack nt (fun ((_,x),_) -> String (list_to_string x)) in
    nt;;

  let nt_char =
    let nt = word "#\\" in
    let nt1 = pack (caten nt (range '!' 'z')) (fun (_,x) -> Char x) in

    let nt2 = pack (caten nt (word_ci "newline")) (fun (_,_) -> Char (Char.chr 10)) in

    let nt3 = pack (caten nt (word_ci "return")) (fun (_,_) -> Char (Char.chr 13)) in

    let nt4 = pack (caten nt (word_ci "tab")) (fun (_,_) -> Char (Char.chr 9)) in

    let nt5 = pack (caten nt (word_ci "page")) (fun (_,_) -> Char (Char.chr 12)) in

    let nt6 = pack (caten nt (word_ci "space")) (fun (_,_) -> Char (Char.chr 32)) in

    disj_list [nt2;nt3;nt4;nt5;nt6;nt1]
  ;;

  let rec nt_pair s =
    let nt1 = pack (caten (char '(') (star nt_sexpr)) (fun (_,x) -> x) in

    let nt2 = pack (char ')') (fun _ -> Nil) in

    let nt1 = caten nt1 nt2 in
    let nt1 = pack nt1 (fun (x,y) -> List.fold_right
                           (fun a b -> Pair(a,b))
                           x
                           y) in

    let nt2 = pack (caten (char '(') (plus nt_sexpr)) (fun (_,x) -> x) in

    let nt3 = caten (char '.') (caten nt_sexpr (char ')')) in
    let nt3 = pack nt3 (fun (_,(x,_)) -> x) in

    let nt2 = caten nt2 nt3 in
    let nt2 = pack nt2 (fun (x,y) -> List.fold_right
                           (fun a b -> Pair(a,b))
                           x
                           y) in

    disj nt1 nt2 s


  and nt_sexpr s  =
    disj_list [(make_spaced nt_vector);(make_spaced nt_pair);
               (make_spaced nt_symbol);
               (make_spaced nt_fraction);
               (make_spaced nt_integer);
               (make_spaced nt_quote_like);
               (make_spaced nt_bool);(make_spaced nt_nil);(make_spaced nt_string);
               (make_spaced nt_char)] s



  and nt_sexpr_line_comment s =
    caten (plus (char ';')) (caten (star (diff nt_any (one_of "\\\n"))) (char '\n')) s

  and nt_sexpr_comment s  =
    caten (word "#;") nt_sexpr s
  and nt_skip s =
    disj_list
      [(pack nt_sexpr_line_comment (fun _ -> true));
       (pack nt_whitespace (fun _ -> true ));
       (pack nt_sexpr_comment (fun _ -> true))] s
  and make_spaced nt s =
    let nt' = star nt_skip in
    let nt = caten nt' (caten nt nt') in
    let nt = pack nt (fun (_,(x,_)) -> x ) in
    nt s

  and nt_nil s =
    pack (caten (char '(') (caten (star nt_skip) (char ')'))) (fun _ -> Nil) s

  and nt_vector s =
    let nt1 = pack (caten (word "#(") (star nt_sexpr)) (fun (_,x) -> x) in

    let nt2 = pack (char ')') (fun _ -> []) in

    let nt1 = caten nt1 nt2 in
    let nt1 = pack nt1 (fun (x,y) -> List.append x y)
    in
    let nt1 = pack nt1 (fun x -> Vector x) in
    nt1 s

  and nt_quote_like s =
    let nt ch name = pack (caten ch nt_sexpr) (fun (_,y) -> Pair(Symbol name,Pair (y,Nil))) in
    disj_list [(nt (char (Char.chr 39)) "quote");
               (nt (char (Char.chr 96)) "quasiquote");
               (nt (word ",@") "unquote-splicing");
               (nt (char (Char.chr 44)) "unquote")] s
  ;;

  let read_sexpr string =
    let list = string_to_list string in
    (fun (x,_) -> x) (nt_sexpr list)
  ;;

  let read_sexprs string =
    let list = string_to_list string in
    let nt = star nt_sexpr in
    (fun (x,_) -> x) (nt list);;

end;; (* struct Parser *)

(* work on the tag parser starts here *)

type expr =
  | Const of sexpr
  | Var of string
  | If of expr * expr * expr
  | Seq of expr list
  | Set of expr * expr
  | Def of expr * expr
  | Or of expr list
  | LambdaSimple of string list * expr
  | LambdaOpt of string list * string * expr
  | Applic of expr * (expr list);;


exception X_syntax_error;;

module type TAG_PARSER = sig
  val read_expression : string -> expr
  val read_expressions : string -> expr list
  val expression_to_string : expr -> string
end;; (* signature TAG_PARSER *)

module Tag_Parser : TAG_PARSER = struct

  let reserved_word_list =
    ["and"; "begin"; "cond"; "define"; "do"; "else";
     "if"; "lambda"; "let"; "let*"; "letrec"; "or";
     "quasiquote"; "quote"; "set!"; "unquote";
     "unquote-splicing"];;  

  let rec process_scheme_list s ret_nil ret_one ret_several =
    match s with
    | Nil -> ret_nil ()
    | (Pair(sexpr, sexprs)) ->
      process_scheme_list sexprs
	(fun () -> ret_one sexpr)
	(fun sexpr' -> ret_several [sexpr; sexpr'])
	(fun sexprs -> ret_several (sexpr :: sexprs))
    | _ -> raise X_syntax_error;;

  let scheme_list_to_ocaml_list args = 
    process_scheme_list args
      (fun () -> [])
      (fun sexpr -> [sexpr])
      (fun sexprs -> sexprs);;

  let expand_let_star ribs sexprs =
    let ribs = scheme_list_to_ocaml_list ribs in
    let params = List.map (function
	| (Pair(name, (Pair(expr, Nil)))) -> name
	| _ -> raise X_this_should_not_happen) ribs in
    let args = List.map
	(function
	  | (Pair(name, (Pair(expr, Nil)))) -> expr
	  | _ -> raise X_this_should_not_happen) ribs in
    let params_set = List.fold_right
	(fun a s ->
	   if (ormap
		 (fun b ->
		    (match (a, b) with
		     | (Symbol a, Symbol b) -> a = b
		     | _ -> raise X_this_should_not_happen))
		 s)
	   then s else a :: s)
	params
	[] in
    let place_holders = List.fold_right
	(fun a s -> Pair(a, s))
	(List.map
	   (fun var -> (Pair(var, (Pair((Bool false), Nil)))))
	   params_set)
	Nil in
    let assignments = List.map2
	(fun var expr ->
	   (Pair((Symbol("set!")),
		 (Pair(var, (Pair(expr, Nil)))))))
	params args in
    let body = List.fold_right
	(fun a s -> Pair(a, s))
	assignments
	sexprs in
    (Pair((Symbol("let")), (Pair(place_holders, body))));;

  let expand_letrec ribs sexprs =
    let ribs = scheme_list_to_ocaml_list ribs in
    let params = List.map (function
	| (Pair(name, (Pair(expr, Nil)))) -> name
	| _ -> raise X_this_should_not_happen) ribs in
    let args = List.map
	(function
	  | (Pair(name, (Pair(expr, Nil)))) -> expr
	  | _ -> raise X_this_should_not_happen) ribs in
    let ribs = List.map
	(function
	  | (Pair(name, (Pair(expr, Nil)))) ->
	    (Pair(name, (Pair(Bool false, Nil))))
	  | _ -> raise X_this_should_not_happen)
	ribs in
    let body = List.fold_right
	(fun a s -> Pair(a, s))
	(List.map2
	   (fun var expr ->
	      (Pair((Symbol("set!")),
		    (Pair(var, (Pair(expr, Nil)))))))
	   params args)
	sexprs in
    let ribs = List.fold_right
	(fun a s -> Pair(a, s))
	ribs
	Nil in
    (Pair((Symbol("let")), (Pair(ribs, body))));;

  exception X_unquote_splicing_here_makes_no_sense;;

  let rec expand_qq sexpr = match sexpr with
    | (Pair((Symbol("unquote")), (Pair(sexpr, Nil)))) -> sexpr
    | (Pair((Symbol("unquote-splicing")), (Pair(sexpr, Nil)))) ->
      raise X_unquote_splicing_here_makes_no_sense
    | (Pair(a, b)) ->
      (match (a, b) with
       | ((Pair((Symbol("unquote-splicing")), (Pair(a, Nil)))), b) ->
	 let b = expand_qq b in
	 (Pair((Symbol("append")),
	       (Pair(a, (Pair(b, Nil))))))
       | (a, (Pair((Symbol("unquote-splicing")), (Pair(b, Nil))))) ->
	 let a = expand_qq a in
	 (Pair((Symbol("cons")), (Pair(a, (Pair(b, Nil))))))
       | (a, b) ->
	 let a = expand_qq a in
	 let b = expand_qq b in
	 (Pair((Symbol("cons")), (Pair(a, (Pair(b, Nil)))))))
    | (Vector(sexprs)) ->
      let s = expand_qq (List.fold_right (fun a b -> Pair(a, b)) sexprs Nil) in
      (Pair((Symbol("list->vector")), (Pair(s, Nil))))
    | Nil | Symbol _ -> (Pair((Symbol("quote")), (Pair(sexpr, Nil))))
    | expr -> expr;;

  let tag_parse sexpr =
    let rec run sexpr =
      match sexpr with
      | Bool _ | Number _ | Char _ | String _ -> Const sexpr
      | Pair (Symbol "quote" ,Pair(x,_)) -> Const x
      | Symbol x when (not (List.mem x reserved_word_list)) -> Var x
      | (Pair((Symbol("if")), (Pair(test, (Pair(dit, (Pair(dif, Nil)))))))) ->
        If (run test, run dit, run dif)
      | (Pair((Symbol("if")), (Pair(test, (Pair(dit,Nil)))))) ->
        If (run test, run dit, Const Void)
      | (Pair((Symbol("or")), lst)) ->
        (expand_or lst)
      | (Pair((Symbol("and")),lst)) ->
        (expand_and lst)
      | (Pair((Symbol("lambda")), (Pair(args, body)))) ->
        (expand_lambda args body)
      | (Pair((Symbol("define")), (Pair((Symbol x), (Pair(y, Nil)))))) ->
        Def (Var x, run y)
      | (Pair((Symbol("define")), (Pair((Pair((Symbol x),params)), list)))) ->
        Def (Var x, run (Pair((Symbol("lambda")), (Pair(params,list)))))
      | (Pair((Symbol("quasiquote")), Pair(sexpr,Nil))) ->
        run (expand_qq sexpr)
      | (Pair((Symbol("set!")), (Pair(name,toset)))) ->
        (match toset with
         | (Pair(x, Nil)) -> Set (run name,run x)
         | _ -> Set (run name,run toset))
      | (Pair((Symbol("cond")), lst)) ->
        (expand_cond lst) 
      | (Pair((Symbol("begin")), lst)) ->
        (expand_begin lst)
      | (Pair((Symbol("let")), (Pair(ribs, sexpr)))) ->
        (expand_let ribs sexpr)
      | (Pair((Symbol("let*")), (Pair(ribs, sexprs)))) ->
        run (expand_let_star ribs sexprs)
      | (Pair((Symbol("letrec")), (Pair(ribs, sexprs)))) ->
        run (expand_letrec ribs sexprs)
      | (Pair((Symbol x), y)) when (not (List.mem x reserved_word_list))  ->
        (expand_applic (Var x) y)
      | (Pair((Pair((Symbol("lambda")), (Pair(args,body)))),lst)) ->
        (expand_applic (expand_lambda args body) lst)
      | Nil -> Const Void
      | _ -> raise X_syntax_error

    and expand_or lst =
      match lst with
      | Nil -> Const (Bool false)
      | _ ->
        let or_lst = scheme_list_to_ocaml_list lst in
        let or_lst = List.map run or_lst in
        Or or_lst

    and expand_lambda args body =
      let body_list = scheme_list_to_ocaml_list body in



      let rec flat_begin lst =
        match lst with
        | (Pair((Symbol("begin")),mashu))::rest -> (flat_begin (scheme_list_to_ocaml_list mashu))@(flat_begin rest)
        | [] -> []
        | hd::rest -> hd::(flat_begin rest) 
      in
      
      let rec split_define_and_body lst acc =
        match lst with
        | [(Pair((Symbol("define")),mashu))] -> raise X_syntax_error
        | (Pair((Symbol("define")),(Pair((Pair((Symbol y),params)), list))))::x::rest ->
          (match x with
           | (Pair((Symbol("define")),_)) ->
             (split_define_and_body
                (x::rest)
                ((Pair((Symbol y), (Pair((Pair((Symbol("lambda")), (Pair(params,list)))), Nil))))::acc))
          | _ -> ((Pair((Symbol y), (Pair((Pair((Symbol("lambda")), (Pair(params,list)))), Nil))))::acc , x::(build_body rest)))
          
        | (Pair((Symbol("define")),mashu))::x::rest ->
          (match x with
          | (Pair((Symbol("define")),_)) -> (split_define_and_body (x::rest) (mashu::acc))
          | _ ->(mashu::acc , x::(build_body rest)))
        | hd::rest -> (check_for_define (hd::rest) rest)
        | [] -> raise X_syntax_error
                  
      and check_for_define original lst =
        match lst with
        | (Pair((Symbol("define")),mashu))::rest -> raise X_syntax_error
        | hd::rest -> check_for_define original rest                                            
        | [] -> (body_list,[])

      and build_body lst =
        match lst with
        | [] -> []
        | (Pair((Symbol x),mashu))::_ when x = "define" -> raise X_syntax_error
        | e1::e2 -> [e1] @ (build_body e2)
      in    

      let without_begins = flat_begin body_list in 
      let body_list_new = split_define_and_body without_begins [] in

      let rec ocaml_lst_to_scheme_lst lst =
        match lst with
        | [e] -> (Pair(e,Nil))
        | e::rest -> (Pair(e,(ocaml_lst_to_scheme_lst rest)))
        | [] -> Nil
      in
      let ribs_lst = (fun (x,y) -> x) body_list_new in
      let ribs =  ocaml_lst_to_scheme_lst (List.rev ribs_lst) in
      let sexprs = ocaml_lst_to_scheme_lst ((fun (x,y) -> y) body_list_new) in
                    
      let body_list =
        if (body_list = ribs_lst)
        then body_list
        else [(Pair((Symbol("letrec")), (Pair(ribs, sexprs))))]
      in






      
      match args, body with
      | Nil,Nil -> raise X_syntax_error
      | Nil,body ->
        if (List.length body_list == 1)
        then LambdaSimple ([],run (List.hd body_list))
        else LambdaSimple ([],Seq (List.map run body_list))
      | (Symbol x) , body when body <> Nil ->
        if (List.length body_list == 1)
        then LambdaOpt ([],x,run (List.hd body_list))
        else LambdaOpt ([],x,Seq (List.map run body_list))
      | args , Nil -> raise X_syntax_error
      | args , body  ->
        let args_list = build args in
        let last_element lst = List.nth lst ((List.length lst)-1) in
        let remove_last2 lst =
          List.rev (List.tl (List.tl (List.rev lst))) in
        let vari lst = List.nth lst ((List.length lst)-2) in
        if (List.length body_list == 1)
        then
          if ((last_element args_list) = "")
          then LambdaOpt (remove_last2 args_list,vari args_list,run (List.hd body_list))
          else LambdaSimple (args_list,run (List.hd body_list))
        else
        if ((last_element args_list) = "")
        then LambdaOpt (remove_last2 args_list,vari args_list,Seq (List.map run body_list))
        else LambdaSimple (args_list,Seq (List.map run body_list))
       
 
    and build args =
      match args with
      | Pair((Symbol x,Symbol y)) -> [x;y;""]
      | Pair((Symbol x,Nil)) -> [x]
      | Pair((Symbol x),y) -> (x)::(build y)
      | _ -> raise X_syntax_error

    and expand_applic expr lst =
      let applic_lst = scheme_list_to_ocaml_list lst in
      Applic (expr,List.map run applic_lst)

    and expand_begin lst =
      match lst with
      | Nil -> Const Void
      | _ ->
        let begin_lst = scheme_list_to_ocaml_list lst in
        let begin_lst = List.map run begin_lst in
        if (List.length begin_lst == 1)
        then (List.hd begin_lst)
        else Seq begin_lst
            
    and expand_let ribs sexpr =
      let ribs = scheme_list_to_ocaml_list ribs in
      let body = scheme_list_to_ocaml_list sexpr in
      let params = List.map (function
	  | (Pair(Symbol x, (Pair(expr, Nil)))) -> x
	  | _ -> raise X_this_should_not_happen) ribs in
      let args = List.map
	  (function
	    | (Pair(name, (Pair(expr, Nil)))) -> run expr
	    | _ -> raise X_this_should_not_happen) ribs in
      if ((List.length body) == 1)
      then
        let body1 =
          match sexpr with
          | (Pair(x,Nil)) -> run x 
          | _ -> raise  X_this_should_not_happen in
        Applic (LambdaSimple(params,body1),args)
      else Applic (LambdaSimple(params,(Seq (List.map run body))),args)

    and  expand_cond sexpr =      
      let lst = scheme_list_to_ocaml_list sexpr in
      let rec run4 lst =
        match lst with
        | [(Pair (Symbol "else", dif))] when dif <> Nil ->
          let dif_lst = scheme_list_to_ocaml_list dif in
          if (List.length dif_lst == 1)
          then (run (List.hd dif_lst))
          else (expand_begin dif)
        | [(Pair (test, dit))] ->
          let dit_lst = scheme_list_to_ocaml_list dit in
          if (dit == Nil)
          then If (run test, Const(Void), Const(Void))
          else if (List.length dit_lst > 1)
          then If (run test, run (Pair((Symbol("begin")), dit)), Const(Void))
          else If (run test, run (List.hd dit_lst), Const(Void))   
        | (Pair (test, dit))::rest ->
          let dit_lst = scheme_list_to_ocaml_list dit in
          if (dit == Nil)
          then If (run test, Const Void, run4 rest)
          else if (List.length dit_lst > 1)
          then If (run test, run (Pair((Symbol("begin")), dit)), run4 rest)
          else If (run test, run (List.hd dit_lst),run4 rest)
        | _ -> raise X_syntax_error
      in
      run4 lst
    and expand_and lst =
      match lst with
      | Nil -> Const (Bool true)
      | _   ->
        let and_lst = scheme_list_to_ocaml_list lst in
        let rec run5 and_lst =
          match and_lst with
          | [and1] ->
            If (Const(Bool true),run and1,Const(Bool false))
          | [and1;and2] ->
            If(run and1,run and2,Const(Bool false))
          | (and1)::rest ->
            If (run and1,run5 rest,Const(Bool false))
          | _ -> raise X_this_should_not_happen
        in
        run5 and_lst
    in
    run sexpr 
  ;;


  let read_expression string = tag_parse (Parser.read_sexpr string);;

  let read_expressions string = List.map tag_parse (Parser.read_sexprs string);;

  let expression_to_string expr =
    let rec run expr =
      match expr with
      | Const x ->  Sexpr.sexpr_to_string x
      | Var x -> x
      | If(test,dit,dif) ->
        "(if " ^ (run test) ^ " " ^ (run dit) ^ " " ^ (run dif) ^ ")"
      | Seq lst ->
        if (List.length lst == 0)
        then "(begin)"
        else "(begin " ^ (String.concat " " (List.map run lst)) ^ ")"
      | Set(expr,expr2) -> "(set! " ^ (run expr) ^ " " ^ (run expr2) ^ ")"
      | Def(id,expr) -> "(define " ^ (run id) ^ " " ^ (run expr) ^ ")"
      | Or lst ->
        if (List.length lst == 0)
        then "#f"
        else "(or " ^ (String.concat " " (List.map run lst)) ^ ")"
      | LambdaSimple(string_lst,expr) ->
        if (List.length string_lst == 0)
        then "(lambda () " ^ (run expr) ^ ")"
        else "(lambda (" ^ (String.concat " " string_lst) ^ ") " ^ (run expr) ^ ")"
      | LambdaOpt(string_lst,str,expr) ->
        if (List.length string_lst == 0)
        then "(lambda " ^ str ^ " " ^ (run expr) ^ ")" 
        else "(lambda (" ^ (String.concat " " string_lst) ^ " . " ^ str ^ ") " ^ (run expr) ^ ")"
      | Applic (expr,expr_lst) ->
        if (List.length expr_lst == 0)
        then "(" ^ (run expr) ^ ")"
        else "(" ^ (run expr) ^ " " ^ (String.concat " " (List.map run expr_lst)) ^ ")"
        (*| _ -> raise X_syntax_error *)

    in
    run expr
  ;;

end;; (* struct Tag_Parser *)

let test_parser string =
  let expr = Tag_Parser.read_expression string in
  let string' = (Tag_Parser.expression_to_string expr) in
  Printf.printf "%s\n" string';;

type var = 
  | VarFree' of string
  | VarParam' of string * int
  | VarBound' of string * int * int;;

type expr' =
  | Const' of sexpr
  | Var' of var
  | Box' of var
  | BoxGet' of var
  | BoxSet' of var * expr'
  | If' of expr' * expr' * expr'
  | Seq' of expr' list
  | Set' of expr' * expr'
  | Def' of expr' * expr'
  | Or' of expr' list
  | LambdaSimple' of string list * expr'
  | LambdaOpt' of string list * string * expr'
  | Applic' of expr' * (expr' list)
  | ApplicTP' of expr' * (expr' list);;

module type SEMANTICS = sig
  val run_semantics : expr -> expr'
  val annotate_lexical_addresses : expr -> expr'
  val annotate_tail_calls : expr' -> expr'
  val box_set : expr' -> expr'
end;;

module Semantics : SEMANTICS = struct

  let rec index_of elem lst i =
    match lst with
    | hd::rest ->
      if hd = elem
      then i
      else index_of elem rest (i + 1)
    | [] -> raise X_this_should_not_happen
  ;;

  let rec search_major var lst_of_lst i =
    match lst_of_lst with
    | hd::rest ->
      if (List.mem var hd)
      then i
      else (search_major var rest (i + 1))
    | [] -> raise X_this_should_not_happen
  ;;

  let rec search_minor var lst_of_lst =
    match lst_of_lst with
    | hd::rest ->
      if (List.mem var hd)
      then (index_of var hd 0)
      else (search_minor var rest)
    | [] -> raise X_this_should_not_happen
  ;;
  
  let annotate_lexical_addresses e =
    let rec analyse ast scope params =
      match ast with
      | Const x -> Const' x
      | If(e1, e2, e3) -> If'((analyse e1 scope params),(analyse e2 scope params),(analyse e3 scope params))
      | Seq(e_lst) -> Seq'(List.map (fun x -> analyse x scope params) e_lst)
      | Set(e1, e2) -> Set'((analyse e1 scope params),(analyse e2 scope params))
      | Def(e1, e2) -> Def'((analyse e1 scope params),(analyse e2 scope params))
      | Or(e_lst) -> Or'(List.map (fun x -> analyse x scope params) e_lst)
      | LambdaSimple(string_lst, expr) ->
        let scope = ([string_lst] @ scope) in
        let params = string_lst in
        LambdaSimple'(string_lst,(analyse expr scope params))
      | LambdaOpt(string_lst,string, expr) ->
        let scope = ([string_lst @ [string]] @ scope) in
        let params = string_lst @ [string] in
        LambdaOpt'(string_lst,string,(analyse expr scope params))
      | Var(var) ->
        if List.mem var params
        then Var'(VarParam'(var,index_of var params 0))
        else if List.mem var (List.flatten scope)
        then
          let major = (search_major var scope 0) - 1 in
          let minor = search_minor var scope in
          Var'(VarBound'(var,major,minor)) 
        else Var'(VarFree'(var))
      | Applic(expr,expr_lst) ->
        let expr' = analyse expr scope params in
        let expr_lst' = List.map (fun x -> analyse x scope params) expr_lst in
        Applic'(expr',expr_lst')
    in
    analyse e [] [] 
  ;;

  let rdc_rac lst =
    let last = List.hd (List.rev lst) in
    let all_but_last = List.rev (List.tl (List.rev lst)) in
    (all_but_last,last)
  ;;

  let annotate_tail_calls e =
    let rec run e in_tail = 
      match e with
      | Const' _ | Var' _ -> e
      | If'(test, dit, dif) ->
        If' (run test false, run dit in_tail, run dif in_tail)
      | Or' exprs ->
        let (all_but_last, last) = rdc_rac exprs in
        Or'((List.map (fun e -> run e false) all_but_last )@[(run last in_tail)])
      | Def'(e1,e2) -> Def'(e1,run e2 false)
      | Seq' expers ->
        let (all_but_last, last) = rdc_rac expers in
        Seq' ((List.map (fun e -> run e false) all_but_last) @ [(run last in_tail)])
      | Set'(e1,e2) -> Set'(e1,run e2 false)
      | Applic'(proc, args) ->
        if in_tail
        then ApplicTP'((run proc false), (List.map (fun e -> run e false) args))
        else Applic'((run proc false), (List.map (fun e -> run e false) args))
      | LambdaSimple' (params,expr) -> LambdaSimple' (params, run expr true)
      | LambdaOpt' (params,param,expr) -> LambdaOpt' (params,param, run expr true)
      | _ -> raise X_not_yet_implemented

    in
    run e false
  ;;

  let var_string var =
    match var with
    | (Var' (VarBound'(x,_,_))) -> x
    | (Var'(VarParam'(x,_))) -> x
    | _ -> raise X_not_yet_implemented
  ;;


  let rec is_get var_string e =
    match e with
    | Def'(e1,e2) ->  (is_get var_string e1) || (is_get var_string e2)
    | LambdaSimple'(param_lst,e) ->
      if (List.mem var_string param_lst)
      then false
      else (is_get var_string e)
    | LambdaOpt'(param_lst,param,e) ->
      if (List.mem var_string (param::param_lst))
      then false
      else (is_get var_string e)
    | ApplicTP' (e,e_lst) ->
      let bool_lst = List.map (fun x -> is_get var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else (is_get var_string e)
    | Applic' (e,e_lst) ->
      let bool_lst = List.map (fun x -> is_get var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else (is_get var_string e)
    | Set'(_,e) ->
      is_get var_string e
    | Seq' (e_lst) ->
      let bool_lst = List.map (fun x -> is_get var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else false
    | Or' (e_lst) ->
      let bool_lst = List.map (fun x -> is_get var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else false
    | If' (e1,e2,e3) -> (is_get var_string e1) || (is_get var_string e2) || (is_get var_string e3)
    | Var' (VarBound'(x,_,_)) | Var'(VarParam'(x,_)) ->
      if (var_string = x)
      then true
      else false
    | _ -> false
  ;;
  let rec is_bound var_string e =
    match e with
    | Def'(e1,e2) ->  (is_bound var_string e1) || (is_bound var_string e2)
    | LambdaSimple'(param_lst,e) ->
      if (List.mem var_string param_lst)
      then false
      else (is_bound var_string e)
    | LambdaOpt'(param_lst,param,e) ->
      if (List.mem var_string (param::param_lst))
      then false
      else (is_bound var_string e)
    | ApplicTP' (e,e_lst) ->
      let bool_lst = List.map (fun x -> is_bound var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else (is_bound var_string e)
    | Applic' (e,e_lst) ->
      let bool_lst = List.map (fun x -> is_bound var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else (is_bound var_string e)
    | Set'(e1,e2) ->
      (is_bound var_string e1) || (is_bound var_string e2)
    | Seq' (e_lst) ->
      let bool_lst = List.map (fun x -> is_bound var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else false
    | Or' (e_lst) ->
      let bool_lst = List.map (fun x -> is_bound var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else false
    | If' (e1,e2,e3) -> (is_bound var_string e1) || (is_bound var_string e2) || (is_bound var_string e3)
    | Var' (VarBound'(x,_,_)) -> if (x = var_string) then true else false
    | _ -> false
  ;;         
  let rec is_set var_string e =
    match e with
    | Def'(_,e) ->  (is_set var_string e)
    | LambdaSimple'(param_lst,e) ->
      if (List.mem var_string param_lst)
      then false
      else (is_set var_string e)
    | LambdaOpt'(param_lst,param,e) ->
      if (List.mem var_string (param::param_lst))
      then false
      else (is_set var_string e)
    | ApplicTP' (e,e_lst) ->
      let bool_lst = List.map (fun x -> is_set var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else is_set var_string e
    | Applic' (e,e_lst) ->
      let bool_lst = List.map (fun x -> is_set var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else is_set var_string e
    | Set'(e,_) ->
      (match e with
       | Var'(VarParam'(x,_)) -> if (x = var_string) then true else false
       | Var'(VarBound'(x,_,_)) -> if (x = var_string) then true else false
       | _ -> raise X_not_yet_implemented)
    | Seq' (e_lst) ->
      let bool_lst = List.map (fun x -> is_set var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else false
    | If' (e1,e2,e3) -> (is_set var_string e1) || (is_set var_string e2) || (is_set var_string e3)
    | Or' (e_lst) ->
      let bool_lst = List.map (fun x -> is_set var_string x) e_lst in
      if (List.mem true bool_lst)
      then true 
      else false
    | _ -> false
  ;;


  let should_box param_lst e =
    List.filter (fun x -> (is_get x e) && (is_set x e) && (is_bound x e)) param_lst
  let box param minor =
    Set' (Var' (VarParam' (param, minor)), Box' (VarParam' (param, minor)))
  ;;

  let to_var e =
    match e with
    | Var' e -> e
    | _ -> raise X_this_should_not_happen
  ;;


  let box_set e =
    let rec run e box_lst boxed =
      match e with
      | Def'(e1,e2) -> Def'(e1,run e2 box_lst boxed)
      | LambdaSimple'(param_lst,e) ->
        let to_box_lst = should_box param_lst e in
        if (to_box_lst) = []
        then LambdaSimple'(param_lst,run e box_lst boxed) 
        else
          let to_box_lst1 = List.map (fun x -> box x (index_of x param_lst 0)) to_box_lst in
          LambdaSimple'(param_lst,Seq'((to_box_lst1 @ [run e to_box_lst (to_box_lst @ box_lst)] )))
      | LambdaOpt'(param_lst,param,e) ->
        let to_box_lst = should_box (param_lst @ [param]) e in
        if (to_box_lst) = []
        then LambdaOpt'(param_lst,param,run e box_lst boxed) 
        else
          let to_box_lst1 = List.map (fun x -> box x (index_of x (param_lst @ [param]) 0)) to_box_lst in
          LambdaOpt'(param_lst,param,run (Seq'((to_box_lst1 @ [run e to_box_lst (to_box_lst @ box_lst)] ))) box_lst boxed)     
      | ApplicTP' (e,e_lst) -> ApplicTP'((run e box_lst boxed),(List.map (fun x -> run x box_lst boxed) e_lst))
      | Applic' (e,e_lst) -> Applic'((run e box_lst boxed),(List.map (fun x -> run x box_lst boxed) e_lst))
      | Set'(e,e1) ->
        (match e with
         | (Var' (VarBound'(y,_,_))) ->
           if (List.mem y boxed) then BoxSet'(((fun x -> (to_var x))e),(run e1 box_lst boxed)) else Set'(e,run e1 box_lst boxed)
         | (Var' (VarParam'(y,_))) ->
           if (List.mem y box_lst) then BoxSet'(((fun x -> (to_var x))e), (run e1 box_lst boxed)) else Set'(e,run e1 box_lst boxed)
         | _ ->  Set'(e,run e1 box_lst boxed))
      | Seq'(e_lst) -> Seq'((List.map (fun x -> run x box_lst boxed) e_lst))
      | If'(e1,e2,e3) -> If'(run e1 box_lst boxed,run e2 box_lst boxed,run e3 box_lst boxed)
      | Or'(e_lst) -> Or'((List.map (fun x -> run x box_lst boxed) e_lst))
      | Var' x ->
        (match x with
         | (VarBound'(y,_,_)) -> if (List.mem y boxed) then BoxGet' x else Var' x 
         | (VarParam'(y,_)) ->   if (List.mem y box_lst) then BoxGet' x else Var' x
         | _ -> Var' x)
      | Const' x -> Const' x
      | _ -> e
    in
    run e [] []
  ;;


  let run_semantics expr =
    box_set
      (annotate_tail_calls
         (annotate_lexical_addresses expr));;

end;; (* struct Semantics *)

let file_to_string input_file =
  let in_channel = open_in input_file in
  let rec run () =
    try 
      let ch = input_char in_channel in ch :: (run ())
    with End_of_file ->
      ( close_in in_channel;
        [] )
  in
  list_to_string (run ())
;;

let string_to_file output_file out_string =
  let out_channel = open_out output_file in
  ( output_string out_channel out_string;
    close_out out_channel );;

let add_sexpr sexpr =
  let rec add sexpr =
    match sexpr with
    | Pair(sexpr1,sexpr2) -> (add sexpr1) @ (add sexpr2) @ [sexpr]
    | Vector(sexpr_list) ->   List.concat(List.map add sexpr_list) @ [sexpr]
    | Symbol(str) -> [String(str);sexpr]
    | _ -> [sexpr]
  in
  add sexpr
;;

let build_const_lst expr' =
  let rec get_inside expr' = 
      match expr' with
      | Const' sexpr ->  add_sexpr sexpr
      | BoxSet'(_ ,expr') -> get_inside expr'
      | If'(expr1',expr2',expr3') -> (get_inside expr1') @ (get_inside expr2') @ (get_inside expr3')
      | Seq'(expr'_list) ->  List.concat(List.map get_inside expr'_list)
      | Set'(expr1',expr2') -> (get_inside expr1') @ (get_inside expr2')
      | Def'(expr1',expr2') -> (get_inside expr1') @ (get_inside expr2')
      | Or'(expr'_list) ->  List.concat(List.map get_inside expr'_list)
      | LambdaSimple'(_,expr') -> get_inside expr'
      | LambdaOpt'(_,_,expr') ->  get_inside expr'
      | Applic'(expr',expr'_list) ->  (get_inside expr') @ List.concat(List.map get_inside expr'_list)
      | ApplicTP'(expr',expr'_list) ->  (get_inside expr') @ List.concat(List.map get_inside expr'_list)
      | _ -> []
  in
  get_inside expr'
;;
  
let rec remove_duplicates list=
  match list with 
  | [] -> []
  | hd::rest -> hd::(remove_duplicates (List.filter (fun x -> x <> hd) rest))
;;


let t_void    = 937610;;
let t_nil     = 722689;;
let t_bool    = 741553;;
let t_char    = 181048;;
let t_integer = 945311;;
let t_string  = 799345;;
let t_symbol  = 368031;;
let t_pair   = 885397;;
let t_vector  = 335728;;

let search sexpr lst =
 (fun (x,y,z) -> y) (List.hd(List.filter (fun (x,y,z) -> x = sexpr) lst))
;;

let search_list sexpr_lst lst =
  List.map (fun x -> search x lst) sexpr_lst
;;

let chars_codes str =
  let char_list = string_to_list str in
  List.map Char.code char_list
;;
  

let const_table sexpr_lst =
  let rec run sexpr_lst curr_lst counter =
    match sexpr_lst with
    | (Void as sexpr)::rest   -> (run rest ((sexpr,counter,[t_void])::curr_lst) (counter + 1))
    | Nil as sexpr::rest    -> (run rest ((sexpr,counter,[t_nil])::curr_lst) (counter + 1))
    | Bool false as sexpr::rest  -> (run rest ((sexpr,counter, [t_bool;0])::curr_lst) (counter + 2))
    | Bool true as sexpr::rest  -> (run rest ((sexpr, counter,[t_bool;1])::curr_lst) (counter + 2))
    | Number(Int number) as sexpr::rest -> (run rest ((sexpr,counter,[t_integer;number])::curr_lst) (counter + 2))
    | Number(Fraction {numerator; denominator}) as sexpr::rest ->
      (run rest ((sexpr, counter,[t_integer;numerator;denominator])::curr_lst) (counter + 3))
    | Char char as sexpr::rest  -> (run rest ((sexpr,counter,[t_char])::curr_lst) (counter + 2))
    | String str as sexpr::rest ->
      (run rest ((sexpr,counter,[t_string;String.length str] @ (chars_codes str))::curr_lst) (counter + 2 + (String.length str)))
    | Symbol str as sexpr::rest -> (run rest ((sexpr,counter,[t_symbol;(search (String str) curr_lst)])::curr_lst) (counter + 2))
    | Pair(sexpr1,sexpr2) as sexpr::rest  ->
      (run rest ((sexpr,counter, [t_pair;(search sexpr1 curr_lst);(search sexpr2 curr_lst)])::curr_lst) (counter + 3))
    | Vector sexpr_lst as sexpr::rest ->
      (run rest ((sexpr,counter,[t_vector;(List.length sexpr_lst)] @ (search_list sexpr_lst curr_lst))::curr_lst) (counter + 2 + (List.length sexpr_lst)))
    | [] -> (counter_ref := counter); List.rev curr_lst
  in
  run sexpr_lst [] !counter_ref
;;

let build_const_table expr'_lst =
  let const_lst = List.concat (List.map build_const_lst expr'_lst) in
  let const_lst = [Void;Nil;(Bool false);(Bool true)] @ const_lst in
  let const_lst = remove_duplicates const_lst in
  let const_table = const_table const_lst in
  const_table
;;

let out_prologue  = 
   file_to_string "prologue.c" 
;;

let out_epilogue  = 
  file_to_string "epilogue.c"
;;

let compile_scheme_file scm_source_file asm_target_file =
  let scm_string = file_to_string scm_source_file in
  let expr_list =  Tag_Parser.read_expressions scm_string in
  let expr'_list_semantics = List.map Semantics.run_semantics expr_list in
  let constants_table = build_const_table expr'_list_semantics in
  let prologue = out_prologue   in
  let epilogue = out_epilogue  in
  let final_string = String.concat "\n" [prologue ; epilogue ] in
  string_to_file asm_target_file final_string;
  counter_ref := 200
;;

let test_run = compile_scheme_file "b.scm" "output.c";;
